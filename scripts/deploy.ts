/* global ethers */
/* eslint prefer-const: "off" */

import * as fs from "fs"
import verify from "../helper-function"

import { ContractReceipt, Transaction,Contract } from "ethers";
import { getAddress, TransactionDescription, TransactionTypes } from "ethers/lib/utils";
import { ethers ,network} from "hardhat";
import { DiamondCutFacet,GovernanceToken } from "../typechain-types";
import { getSelectors, FacetCutAction } from "./libraries/diamond";
import { BoxFacet } from "../typechain-types/BoxFacet";
import { MIN_DELAY,
  QUORUM_PERCENTAGE,
  VOTING_PERIOD,developmentChains,
  VOTING_DELAY ,ADDRESS_ZERO,FUNC,PROPOSAL_DESCRIPTION,NEW_STORE_VALUE,proposalsFile} from "../helper-hardhat-config"
import { GovernorContract } from "../typechain-types";
import { propose } from "./propose";
import { vote } from "./vote";
import { queueAndExecute } from "./queue-and-execute";

export let DiamondAddress: string;
export let governorContractAddress: string;



export async function deployDiamond() {
  const accounts = await ethers.getSigners();
  const contractOwner = accounts[0];

  console.log("woner",contractOwner);

  // deploy DiamondCutFacet
  const DiamondCutFacet = await ethers.getContractFactory("DiamondCutFacet");
  const diamondCutFacet = await DiamondCutFacet.deploy();
  await diamondCutFacet.deployed();
  console.log("DiamondCutFacet deployed:", diamondCutFacet.address);

  // deploy Diamond
  const Diamond = await ethers.getContractFactory("Diamond");
  const diamond = await Diamond.deploy(
    contractOwner.address,
    diamondCutFacet.address
  );
  await diamond.deployed();
  console.log("Diamond deployed:", diamond.address);

  // deploy DiamondInit
  // DiamondInit provides a function that is called when the diamond is upgraded to initialize state variables
  // Read about how the diamondCut function works here: https://eips.ethereum.org/EIPS/eip-2535#addingreplacingremoving-functions
  const DiamondInit = await ethers.getContractFactory("DiamondInit");
  const diamondInit = await DiamondInit.deploy();
  await diamondInit.deployed();
  console.log("DiamondInit deployed:", diamondInit.address);

  // deploy facets
  console.log("");
  console.log("Deploying facets");
  const FacetNames = ["DiamondLoupeFacet", "OwnershipFacet","BoxFacet","GovernanceToken"];
  const cut = [];
  for (const FacetName of FacetNames) {
    const Facet = await ethers.getContractFactory(FacetName);
    const facet = await Facet.deploy();
    await facet.deployed();
    console.log(`${FacetName} deployed: ${facet.address}`);
    cut.push({
      facetAddress: facet.address,
      action: FacetCutAction.Add,
      functionSelectors: getSelectors(facet),
    });
  }

  // upgrade diamond with facets
  console.log("");
  console.log("Diamond Cut:", cut);
  const diamondCut = (await ethers.getContractAt(
    "IDiamondCut",
    diamond.address
  )) as DiamondCutFacet;
  let tx;
  let receipt: ContractReceipt;
  // call to init function
  let functionCall = diamondInit.interface.encodeFunctionData("init");
  tx = await diamondCut.diamondCut(cut, diamondInit.address, functionCall);
  console.log("Diamond cut tx: ", tx.hash);
  receipt = await tx.wait();
  if (!receipt.status) {
    throw Error(`Diamond upgrade failed: ${tx.hash}`);
  }
  console.log("Completed diamond cut");
  DiamondAddress = diamond.address;


  const TimeLock = await ethers.getContractFactory("TimeLock");
    const timeLock = await TimeLock.deploy(3600,[],[]);
    await timeLock.deployed();

          const delegatet = (await ethers.getContractAt(
            "GovernanceToken",
            DiamondAddress
          )) as GovernanceToken;
          await delegatet.delegate(contractOwner.address);


          console.log("swag!",await delegatet.mint(contractOwner.address,1000000));
    
          const transfer = (await ethers.getContractAt(
            "BoxFacet",
            DiamondAddress
          )) as BoxFacet;
         const transferTx = await transfer.changeOwner(timeLock.address);
          await transferTx.wait(1)


        //   const transfer2 = (await ethers.getContractAt(
        //     "BoxFacet",
        //     DiamondAddress
        //   )) as BoxFacet;
        //  const transferTx2 = await transfer2.retrieveOwner();
        //   console.log("cutie")
        //   console.log(transferTx2);
        //   console.log(timeLock.address);



  

    console.log("----------------------------------------------------")
    console.log("Setting up contracts for roles...")
    // would be great to use multicall here...
    const proposerRole = await timeLock.PROPOSER_ROLE()
    const executorRole = await timeLock.EXECUTOR_ROLE()
    const adminRole = await timeLock.TIMELOCK_ADMIN_ROLE()



    console.log("----------------------------------------------------")
    console.log("Deploying GovernorContract and waiting for confirmations...")


    const GovernorContract = await ethers.getContractFactory("GovernorContract");
    const governorContract = await GovernorContract.deploy(
        delegatet.address,
        timeLock.address,
        QUORUM_PERCENTAGE,
        VOTING_PERIOD,
        VOTING_DELAY,
    );
    await governorContract.deployed();

    governorContractAddress = governorContract.address

    if (!developmentChains.includes(network.name) && process.env.ETHERSCAN_API_KEY) {
      await verify(governorContract.address, [])
    }


    console.log('timelockadress',timeLock.address)
   
   console.log(`GovernorContract at ${governorContract.address}`)

    const governor = (await ethers.getContractAt(
      "GovernorContract",
      governorContract.address
    )) as GovernorContract;

  
    const proposerTx = await timeLock.grantRole(proposerRole, governor.address)
     await proposerTx.wait(1)
    const executorTx = await timeLock.grantRole(executorRole, ADDRESS_ZERO)
    await executorTx.wait(1)
    const revokeTx = await timeLock.revokeRole(adminRole,contractOwner.address)
    await revokeTx.wait(1)




  
   
  

  //  const proposals = JSON.parse(fs.readFileSync(proposalsFile, "utf8"))
  //  // Get the last proposal for the network. You could also change it for your index
  //  const proposalId = proposals[network.config.chainId!].at(-1);
  //  // 0 = Against, 1 = For, 2 = Abstain for this example
  //  const voteWay = 1
  //  const reason = "I lika do da cha cha"
  //   await vote(proposalId, voteWay, reason)
  // console.log("Delegated!")

  //await deployxd();
}

async function scripts(){


 await propose([NEW_STORE_VALUE], FUNC, PROPOSAL_DESCRIPTION, DiamondAddress, governorContractAddress);
  await vote(governorContractAddress);
  await queueAndExecute(DiamondAddress);



  // fix this tjhat it shouldnt be possible
//   const transfer = (await ethers.getContractAt(
//     "BoxFacet",
//     DiamondAddress
//   )) as BoxFacet;
//  const transferTx = await transfer.store(5);
//  const transferTx3 = await transfer.store(8);

//  const transferTx2 = await transfer.retrieve();
//  console.log(transferTx2);
 




  

}

// We recommend this pattern to be able to use async/await everywhere
// and properly handle errors.
if (require.main === module) {
  deployDiamond()
    .then(() => scripts().then( () => process.exit(0)))
    .catch((error) => {
      console.error(error);
      process.exit(1);
    });
}

exports.deployDiamond = deployDiamond;

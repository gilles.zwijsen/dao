import {
    getSelectors,
    FacetCutAction,
    removeSelectors,
    findAddressPositionInFacets,
  } from "../scripts/libraries/diamond";
  import {
      BoxFacet,
    DiamondCutFacet,
    DiamondLoupeFacet,
    OwnershipFacet,
    Test1Facet,
  } from "../typechain-types";
  
  import { deployDiamond, DiamondAddress } from "../scripts/deploy";
  import { FacetStructOutput } from "../typechain-types/DiamondLoupeFacet";
  
  import { ethers } from "hardhat";
  
  import { ContractReceipt } from "ethers";
  import { assert } from "chai";


  let diamondCutFacet: DiamondCutFacet;
  let diamondLoupeFacet: DiamondLoupeFacet;
  let ownershipFacet: OwnershipFacet;
  let tx;
  let receipt: ContractReceipt;
  let result;
  const addresses: string[] = [];


  

  export async function test() {

    await deployDiamond();
    diamondCutFacet = await ethers.getContractAt(
      "DiamondCutFacet",
      DiamondAddress
    );
    diamondLoupeFacet = await ethers.getContractAt(
      "DiamondLoupeFacet",
      DiamondAddress
    );
    ownershipFacet = await ethers.getContractAt(
      "OwnershipFacet",
      DiamondAddress
    );



    console.log("saying hello...")


    //should add test1
    const Test1Facet = await ethers.getContractFactory("Test1Facet");
    const test1Facet = await Test1Facet.deploy();
    await test1Facet.deployed();
    console.log(`test1Facet deployed: ${test1Facet.address}`);


    addresses.push(test1Facet.address);
    const selectors = removeSelectors(getSelectors(test1Facet), [
      " supportsInterface(bytes4)",
    ]);

    tx = await diamondCutFacet.diamondCut(
      [
        {
          facetAddress: test1Facet.address,
          action: FacetCutAction.Add,
          functionSelectors: selectors,
        },
      ],
      ethers.constants.AddressZero,
      "0x",
      { gasLimit: 800000 }
    );
    receipt = await tx.wait();
    if (!receipt.status) {
      throw Error(`Diamond upgrade failed: ${tx.hash}`);
    }
    result = await diamondLoupeFacet.facetFunctionSelectors(test1Facet.address);
    assert.sameMembers(result, selectors);


    const test1Facett = (await ethers.getContractAt(
        "Test1Facet",
        DiamondAddress
      )) as Test1Facet;
      await test1Facet.test1Func10();

    //   const test2Func1 = await test1Facett.test1Func1();
    //   console.log( `dwa : ${test2Func1} `)


    //   replace supportinterface

    // const t1facet = await ethers.getContractFactory("Test1Facet");
    // const test1Facettt = await ethers.getContractFactory("Test1Facet");
    // const selectorsss = getSelectors(t1facet).get(["supportsInterface(bytes4)"]);
    // const testFacetAddress = addresses[3];
    // tx = await diamondCutFacet.diamondCut(
    //   [
    //     {
    //       facetAddress: testFacetAddress,
    //       action: FacetCutAction.Replace,
    //       functionSelectors: selectorsss,
    //     },
    //   ],
    //   ethers.constants.AddressZero,
    //   "0x",
    //   { gasLimit: 800000 }
    // );
    // receipt = await tx.wait();
    // if (!receipt.status) {
    //   throw Error(`Diamond upgrade failed: ${tx.hash}`);
    // }
    // result = await diamondLoupeFacet.facetFunctionSelectors(testFacetAddress);
    // assert.sameMembers(result, getSelectors(test1Facettt));


    //add test2

    const Test2Facet = await ethers.getContractFactory("Test2Facet");
    const test2Facet = await Test2Facet.deploy();
    await test2Facet.deployed();
    console.log(`test2Facet deployed: ${test2Facet.address}`);

    addresses.push(test2Facet.address);
    const selectorss = getSelectors(test2Facet);
    tx = await diamondCutFacet.diamondCut(
      [
        {
          facetAddress: test2Facet.address,
          action: FacetCutAction.Add,
          functionSelectors: selectorss,
        },
      ],
      ethers.constants.AddressZero,
      "0x",
      { gasLimit: 800000 }
    );
    receipt = await tx.wait();
    if (!receipt.status) {
      throw Error(`Diamond upgrade failed: ${tx.hash}`);
    }
    result = await diamondLoupeFacet.facetFunctionSelectors(test2Facet.address);
    assert.sameMembers(result, selectorss);




    var variable = await test2Facet.test2Func1.call({from: addresses[3], gas: 5000000});
    console.log(variable.toString());

    const test1 = (await ethers.getContractAt(
        "Test1Facet",
        DiamondAddress
      )) as Test1Facet;
      let results = await test1Facet.test1Func1();

      console.log(results);


      const test2 = (await ethers.getContractAt(
        "BoxFacet",
        DiamondAddress
      )) as BoxFacet;
      let result2 = await test2.retrieve();

      console.log( await result2.toString());
      console.log("DONEEEE");




  }


  test()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error)
    process.exit(1)
  })
import * as fs from "fs"
import { network, ethers } from "hardhat"
import { Address } from "hardhat-deploy/types";
import { proposalsFile, developmentChains, VOTING_PERIOD } from "../helper-hardhat-config"
import { DiamondAddress } from "./deploy";
import {moveBlocks} from "../utils/move-blocks"
import { BigNumber } from "ethers"



// 0 = Against, 1 = For, 2 = Abstain for this example
export async function vote(GovernorContract : Address) {
  const proposals = JSON.parse(fs.readFileSync(proposalsFile, "utf8"))
  // Get the last proposal for the network. You could also change it for your index
  const proposalId = proposals[network.config.chainId!].at(-1);
  console.log(proposalId);
  // 0 = Against, 1 = For, 2 = Abstain for this example
  const voteWay = 1
  const reason = "I lika do da cha cha"
  console.log("Voting...")
  const governor = await ethers.getContractAt("GovernorContract",GovernorContract)
  const proposalState2 = await governor.state(proposalId)

  console.log(`Current Proposal State: ${proposalState2}`)

  if (proposalState2 == 1){
    const accounts = await ethers.getSigners();
    const contractOwner = accounts[0];
  

  console.log(await (governor.getVotes(contractOwner.address,17)))
    const voteTx = await governor.castVoteWithReason(proposalId, voteWay,reason)
    const voteTxReceipt = await voteTx.wait(1)
    console.log(voteTxReceipt)
    console.log(voteTxReceipt.events[0].args.reason)
    const proposalState = await governor.state(proposalId)
    console.log(`Current Proposal States: ${proposalState}`)
    if (developmentChains.includes(network.name)) {
      await moveBlocks(VOTING_PERIOD + 1)
  }
  }
}

if (require.main === module) {
vote(DiamondAddress)
.then(() => process.exit(0))
.catch((error) => {
  console.error(error);
  process.exit(1);
});
}

exports.vote = vote;
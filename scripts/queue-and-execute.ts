import { ethers, network } from "hardhat"
import {
  FUNC,
  NEW_STORE_VALUE,
  PROPOSAL_DESCRIPTION,
  MIN_DELAY,
  developmentChains,
} from "../helper-hardhat-config"
import { BoxFacet, GovernorContract } from "../typechain-types"
import { Address } from "hardhat-deploy/types";
import { governorContractAddress } from "./deploy";
import { moveBlocks } from "../utils/move-blocks";
import {moveTime}  from "../utils/move-Time";
import { BigNumber } from "ethers"




export async function queueAndExecute(DiaAdress : Address){


  
  console.log("dwq")

  console.log(DiaAdress)


  console.log(DiaAdress)
  const box = (await ethers.getContractAt(
    "BoxFacet",DiaAdress)) as BoxFacet ;

  
    const args = [NEW_STORE_VALUE]
    const functionToCall = FUNC
    const encodedFunctionCall = box.interface.encodeFunctionData(functionToCall, [NEW_STORE_VALUE] )
    const descriptionHash = ethers.utils.keccak256(ethers.utils.toUtf8Bytes(PROPOSAL_DESCRIPTION))
    console.log(encodedFunctionCall)
  // could also use ethers.utils.id(PROPOSAL_DESCRIPTION)

  

  const governor = (await ethers.getContractAt(
    "GovernorContract","0xB7f8BC63BbcaD18155201308C8f3540b07f84F5e")) as GovernorContract;
  console.log("Queueing...")
  const queueTx = await governor.queue([DiaAdress], [0], [encodedFunctionCall], descriptionHash)
  await queueTx.wait(1)


  

  if (developmentChains.includes(network.name)) {
    console.log("dwadwadwadwad")
    await moveTime(MIN_DELAY + 1)
    await moveBlocks(1)
  }
  
  


  console.log("Executing...")
  // this will fail on a testnet because you need to wait for the MIN_DELAY!
  const executeTx = await governor.execute(
    [DiaAdress],
    [0],
    [encodedFunctionCall],
    descriptionHash
  )
  await executeTx.wait(1)
  console.log(`Box value: ${await box.retrieve()}`)


 

}
if (require.main === module) {
  queueAndExecute("dwa")
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error)
    process.exit(1)
  })
}
  exports.queueAndExecute = queueAndExecute;
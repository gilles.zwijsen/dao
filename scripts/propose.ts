import { ethers, network } from "hardhat"
import {
  developmentChains,
  VOTING_DELAY,
  proposalsFile,
  FUNC,
  PROPOSAL_DESCRIPTION,
  NEW_STORE_VALUE,VOTING_PERIOD
} from "../helper-hardhat-config"
import * as fs from "fs"
import { Address } from "hardhat-deploy/types"
import { DiamondAddress } from "./deploy"
import { GovernorContract } from "../typechain-types"
import {moveBlocks} from "../utils/move-blocks"
import { BigNumber } from "ethers"





export async function propose(args: any[], functionToCall: string, proposalDescription: string, diamondaddress : Address, governorContract : Address) {
  console.log("saw")
  console.log(DiamondAddress)
  console.log(governorContract, "give")
  const governor = await ethers.getContractAt("GovernorContract",governorContract)
  const box = await ethers.getContractAt("BoxFacet",DiamondAddress)
  const encodedFunctionCall = box.interface.encodeFunctionData(functionToCall, args)
  console.log(encodedFunctionCall,"encodedfunctioncall")
  console.log(`Proposing ${functionToCall} on ${box.address} with ${args}`)
  console.log(`Proposal Description:\n  ${proposalDescription}`)
  const proposeTx = await governor.propose(
    [DiamondAddress],
    [0],
    [encodedFunctionCall],
    proposalDescription
  )

  

  // const proposeTx = (await ethers.getContractAt(
  //   "GovernorContract",
  //   governorContract
  // )) as GovernorContract;
  // let result2 = await proposeTx.propose(
  //   [box.address],
  //   [0],
  //   [encodedFunctionCall],
  //   proposalDescription);



 // If working on a development chain, we will push forward till we get to the voting period.
 
  const proposeReceipt = await proposeTx.wait(1)
  const proposalId = proposeReceipt.events[0].args.proposalId
  console.log(`Proposed with proposal ID:\n  ${proposalId}`)
  

  

  
  const proposalState = await governor.state(proposalId)
  const proposalSnapShot = await governor.proposalSnapshot(proposalId)
  const proposalDeadline = await governor.proposalDeadline(proposalId)
  // save the proposalId
  storeProposalId(proposalId);


  // The state of the proposal. 1 is not passed. 0 is passed.
  console.log(`Current Proposal State: ${proposalState}`)
  // What block # the proposal was snapshot
  console.log(`Current Proposal Snapshot: ${proposalSnapShot}`)
  // The block number the proposal voting expires
  console.log(`Current Proposal Deadline: ${proposalDeadline}`)
  if (developmentChains.includes(network.name)) {
    await moveBlocks(VOTING_PERIOD + 1)
  }


}
  
function storeProposalId(proposalId: any) {
  console.log("Dwadwa")
  console.log(DiamondAddress)
  const chainId = network.config.chainId!.toString();
  let proposals:any;

  if (fs.existsSync(proposalsFile)) {
      proposals = JSON.parse(fs.readFileSync(proposalsFile, "utf8"));
  } else {
      proposals = { };
      proposals[chainId] = [];
  }   
  proposals[chainId].push(proposalId.toString());
  fs.writeFileSync(proposalsFile, JSON.stringify(proposals), "utf8");
  
}



if (require.main === module) {
  

  //still fix the last argument for gov address

  propose([NEW_STORE_VALUE], FUNC, PROPOSAL_DESCRIPTION,DiamondAddress,DiamondAddress)
    .then(() => process.exit(0))
    .catch((error) => {
      console.error(error);
      process.exit(1);
    });
}

exports.propose = propose;
// contracts/Box.sol
// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "@openzeppelin/contracts/access/Ownable.sol";

contract BoxFacet  {
  uint256 private value;
  address private owner;
  bool private createdOwner;


  constructor(){
    owner = msg.sender;
    createdOwner = true;

  }


  // Emitted when the stored value changes
  event ValueChanged(uint256 newValue);

  // Stores a new value in the contract
  function store(uint256 newValue) public {
    value = newValue;
    emit ValueChanged(newValue);
  }

  // Reads the last stored value
  function retrieve() public view returns (uint256) {
   // require(owner == msg.sender);
    return value;
  }

  function changeOwner(address newOwner) public {
    require(createdOwner == false);
   //address oldOwner = owner;
    owner = newOwner;
    //emit Ownerset(oldOwner,owner)
     createdOwner = false;
  }

  function retrieveOwner() public view returns(address){
    return owner;
  }

 



  

}
